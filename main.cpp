#include <string>
#include <iostream>

int main()
{
    std::string s;
    
    std::cout << "Input your string: ";
    std::getline(std::cin, s);

    std::cout << "String: " << s << std::endl;
    std::cout << "Lenght: " << s.length() << std::endl;
    std::cout << "First char: " << s[0] << std::endl;
    std::cout << "Last char: " << s[s.length()-1] << std::endl;

    return 0;
}